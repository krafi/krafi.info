import { defineConfig } from 'astro/config';

// https://astro.build/config
// export default defineConfig({});
export default {
    // Other configuration options
    include: "src/**/*.{astro,js,jsx,ts,tsx}", // Include Astro files
  };
  